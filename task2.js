class Person1 {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
    describe() {
        console.log(`${this.name} is ${this.age} years old`);
    }
}

const jack = new Person1('Jack', 25);
const jill = new Person1('Jill', 24);
jack.describe();
jill.describe();