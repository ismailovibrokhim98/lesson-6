class Person {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
}
class Baby extends Person {
    constructor(name, age, favoriteToy) {
     super(name, age);
     this.toy = favoriteToy;
    }
    play() {
        console.log(`${this.name} is ${this.age}and Playing with ${this.toy}`);
    }
}

var babyNew = new Baby('Jack', 4, 'Car');
console.log(babyNew);


